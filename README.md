# OSM Belgium — municipality changes 2019-01-01

For mappers:

For the [WikiProject Belgium/Municipality Fusions](https://wiki.openstreetmap.org/wiki/WikiProject_Belgium/Municipality_Fusions) project you only have to look at [`adressen_gewijzigde_straatnaam_summary.txt`](./adressen_gewijzigde_straatnaam_summary.txt).
This file contains all changing addresses, per municipality.

Use the JOSM tagging and style presets, they will make your life a lot easier!
The tagging preset includes all new names so you have autocompletion.

If you don't know how to these, see the official docs [for tagging presets](https://josm.openstreetmap.de/wiki/Help/Preferences/Map#TaggingPresets) ("Add a preset from the file") and [for styles](https://josm.openstreetmap.de/wiki/Styles#Activatingmorestyles) (Adding own styles).
The URL / File for the tagging preset is `https://framagit.org/Midgard/osm-belgium---municipality-changes-2019-01-01/raw/master/josm_preset_flemish_municipality_fusions.xml`.
The one for the style is `https://framagit.org/Midgard/osm-belgium---municipality-changes-2019-01-01/raw/master/josm_style_flemish_municipality_fusions.mapcss`.

---

More elaborate explanation:

On 1 January 2019, several Flemish municipalities are merging, and postal codes in Antwerp are slightly adjusted.
Our government published [a list of changing addresses](https://overheid.vlaanderen.be/gemeentelijke-fusies#adreswijzigingen-pro-actief-verwerken).
At OpenStreetMap Belgium, we want to be the first general purpose map to incorporate these changes as soon as they're live.

This repository contains scripts and processed data to that end.

The data is provided by Agentschap Informatie Vlaanderen (Agency Information Flanders, AIV for short).
The license is not made explicit, but it's clear that this data is provided for the common good.

Original data downloaded from: https://overheid.vlaanderen.be/gemeentelijke-fusies#adreswijzigingen-pro-actief-verwerken  
AIV data version: 20 December 2018

Description per file follows below.

## Caveats
The agency publishes the following caveats:

> * Er is momenteel nog overleg aan de gang tussen bpost en enkele gemeenten in verband met postcodewijzigingen. Afhankelijk van de uitkomst zouden er begin januari nog enkele adressen een andere postcode kunnen krijgen dan aangegeven in deze verschillijsten:
> 	* Loweg 3 en Loweg 5 in Kruisem
> 	* Spoelstraat 12 en Meirebeekstraat 45 in Deinze
>
> * Inwoners van Veldeken in Nevele hebben bezwaar aangetekend bij de Raad van State tegen de splitsing van Veldeken in meerdere straten en de bijhorende hernummering. Het bezwaar is niet opschortend dus in afwachting van een uitspraak blijft de beslissing van de gemeenteraad geldig. De splitsing van Veldeken wordt dus gewoon uitgevoerd als onderdeel van de fusie van Nevele en Deinze.
>
> * In Oudsbergen zullen in de maand januari nog een hele reeks hernummeringen plaatsvinden door de gemeente. Meer informatie omtrent deze hernummeringen kan je opvragen bij de gemeente: [email addresses censored]
>
> * Weg naar Helchteren 35B en Wilgenstraat 3W in Oudsbergen zijn tijdelijke adressen die om technische redenen opgenomen zijn in de verschillijst. In januari worden beide adressen verwijderd.
>
> * De hernummering van Grote Baan 31, Zomergem naar Veldeken 22A, Lievegem is nog niet 100% zeker.

Midgard also provides some caveats:

* In the change list, four addresses outside Antwerp change postal code (`awk -F';' -e '$5 != $10 && $1 != "Antwerpen" { print }' gewijzigde_adressen.csv`):
	* `Spoelstraat 6, 9800 Deinze` → `Deinze;Spoelstraat;12;;9850`
	* `Nachtegaalstraat 23, 9770 Kruishoutem` → `Leeuwerikstraat 4, 9750 Kruisem`
	* `Nachtegaalstraat 25, 9770 Kruishoutem` → `Leeuwerikstraat 2, 9750 Kruisem`
	* `Meirebeekstraat 45, 9850 Nevele` → `Meirebeekstraat 45, 9800 Deinze`

## Dependencies for generation
You don't need these to use data that's already in the repo.
* \*NIX (Linux, macOS, \*BSD, Solaris…)
* `sed` (pre-installed on most systems)
* [`fex`](https://www.semicomplete.com/projects/fex/)

## gewijzigde_adressen.csv
This file contains all addresses stripped of IDs.
Items where no changes are left, are removed.
The data is sorted by old municipality, old streetname, old housenumber, old bus number.

### How to generate
Start with `Exclusief RR/fusies_adressen.csv` in the source data and do
```bash
echo "O_gemeente;O_straatnaam;O_hnr;O_app/busnr;O_postcode;N_gemeente;N_straatnaam;N_hnr;N_app/busnr;N_postcode" > gewijzigde_adressen.csv
cat fusies_adressen.csv | \
	fex ';{?6:11,19:24}' | \
	# Remove the header. Merge fields 4 (appartementsnummer) and 5 (busnummer), there's no meaningful
	# difference. They are never both in use at the same time and other fields are always populated,
	# so this short sed script does the job
	sed -r '1d;s/;;(.*);;/;\1;/' | \
	# Remove lines where the last 5 fields are equal to the first 5 fields: address doesn't change
	sed -r '/^(([^;]*;){4}[^;]*);\1$/d' | \
	# The first sort on field 4 is to make lines where the field is empty rise, the second sort on
	# field 4 sorts numerically. This is to sort `56;0A` after `56;`.
	sort -t';' -k1,1 -k2,2 -k3,3n -k4,4 -k4,4n \
	>> gewijzigde_adressen.csv
```

## adressen_gewijzigde_straatnaam.csv
This file contains addresses where the street name has changed.
Municipality name, postal code and house number are not taken into account.

### How to generate
Start with `gewijzigde_adressen.csv` generated above and do
```bash
sed -r '/^([^;]+);([^;]+);([^;]+);([^;]*);([^;]+);[^;]+;\2;\3;\4;[^;]+$/d' gewijzigde_adressen.csv > adressen_gewijzigde_straatnaam.csv
```
