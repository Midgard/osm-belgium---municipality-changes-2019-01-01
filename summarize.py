#!/usr/bin/env python

import csv

def row_to_dict(row):
	o_muni, o_street, o_nr, o_unit, o_zip, \
	n_muni, n_street, n_nr, n_unit, n_zip = row
	return {
		"o_muni":   o_muni,
		"o_street": o_street,
		"o_nr":     o_nr,
		"o_unit":   o_unit,
		"o_zip":    o_zip,
		"n_muni":   n_muni,
		"n_street": n_street,
		"n_nr":     n_nr,
		"n_unit":   n_unit,
		"n_zip":    n_zip
	}

def prepend_if_nonempty(s, prependage):
	return prependage + s if s != "" else ""

def print_single(row):
	print("{o_street} {o_nr}{o_unit_f} → {n_street} {n_nr}{n_unit_f}{zip_w}".format(
		o_unit_f=prepend_if_nonempty(row["o_unit"], "/"),
		n_unit_f=prepend_if_nonempty(row["o_unit"], "/"),
		zip_w=(
			". ⚠ changes from ZIP code {o_zip} to {n_zip}".format(**row)
			if row["o_zip"] != row["n_zip"] else ""
		),
		**row
	))

def print_range(begin, end):
	print("{o_street} {begin} — {end} → {n_street} (same №)".format(
		begin=begin["o_nr"] + prepend_if_nonempty(begin["o_unit"], "/"),
		end=end["o_nr"] + prepend_if_nonempty(end["o_unit"], "/"),
		**end
	))

with open("adressen_gewijzigde_straatnaam.csv", "r") as fh:
	headers = next(fh).strip()
	assert headers == "O_gemeente;O_straatnaam;O_hnr;O_app/busnr;O_postcode;N_gemeente;N_straatnaam;N_hnr;N_app/busnr;N_postcode"

	reader = map(row_to_dict, csv.reader(fh, delimiter=";"))
	start_of_current_range = prev = next(reader)
	print("{o_muni} → {n_muni}\n======================".format(**prev).upper())

	for row in reader:
		# Assert that if 2 addresses were in the municipality before, they end up in the same
		# municipality
		assert row["n_muni"] == prev["n_muni"] or row["o_muni"] != prev["o_muni"]

		if not (
			# If large-scale parts of address are the same as in the previous row…
			row["o_muni"]   == prev["o_muni"] and
			row["o_street"] == prev["o_street"] and
			row["o_zip"]    == prev["o_zip"] and
			row["n_muni"]   == prev["n_muni"] and
			row["n_street"] == prev["n_street"] and

			row["o_nr"]     == row["n_nr"] and
			row["o_unit"]   == row["n_unit"] and
			row["o_zip"]    == row["n_zip"] and

			prev["o_nr"]    == prev["n_nr"] and
			prev["o_unit"]  == prev["n_unit"] and
			prev["o_zip"]   == prev["n_zip"]
		):
			if start_of_current_range == prev:
				print_single(prev)
			else:
				print_range(start_of_current_range, prev)
			start_of_current_range = row

			if prev["o_muni"] != row["o_muni"]:
				print("\n{o_muni} → {n_muni}\n======================".format(**row).upper())

		prev = row

	if start_of_current_range == prev:
		print_single(prev)
	else:
		print_range(start_of_current_range, prev)
